"""SYS DIAGNOSTIC GUARDIAN

Tests are classes that inherit from DiagTest.  The name of the class
should be the name of the test (TEST_NAME). They should define a
single test() method:

class TEST_NAME(DiagTest):
    def test(self):
        if test_logic:
            yield MESSAGE

If this method yields a string (MESSAGE), that string is raised as a
guardian "notification" and the following channel will be 'True':

<IFO>:GRD-SYS_DIAG_NOTIFICATION

The test notifications will be in the GRD-SYS_DIAG_USERMSG* channels,
of the form:

TEST_NAME: MESSAGEfrom guardian import clock

"""
# must import the DiagTest class from the main module
from SYS_DIAG import DiagTest
import numpy as np
from cdsutils import avg
#from ezca.ligofilter import LIGOFilter


class MC_WFS(DiagTest):
    """Check MC WFS are engaged"""
    def test(self):
        if ezca['IMC-WFS_GAIN'] == 0:
            yield "not engaged"


class PSL_NOISE_EATER(DiagTest):
    """Check that PSL noise eater is engaged

    """
    nominal = -5852
    def test(self):
        value = ezca['PSL-MIS_NPRO_RRO_OUTPUT']
        if not (self.nominal-50 <= value <= self.nominal+50):
            yield "NPRO output out of range"

class PSL(DiagTest):
    """Checks the PSL for FSS oscillations and ISS diffracted power"""
    def test(self):
        # FSS oscillations
        if abs(ezca['PSL-FSS_FAST_MON_OUTPUT']) >= 7:
            yield 'FSS PZT MON is high, it may be oscillating'
        # ISS diffracted pwr
        diff_pwr = avg(-10, 'PSL-ISS_DIFFRACTION_AVG')
        diff_high = 11
        diff_low = 6
        if diff_pwr >= diff_high:
            yield 'ISS diffracted power is high'
        if diff_pwr <= diff_low:
            yield 'Iss diffracted power is low'

class TCS_LASER(DiagTest):
    """Check TCS laser for:
    Switched ON
    Power above 50W
    No Flow Alarm
    No IR alarm
    
    Only for TCS X for now.
    """
    def test(self):
        # TCSX
        laser_switch_X = ezca['TCS-ITMX_CO2_LASERONOFFSWITCH']
        if not laser_switch_X == 1:
            yield "TCS X is switched OFF"
        # if the power gets below 50 there is a problem
        if not ezca['TCS-ITMX_CO2_LSRPWR_HD_PD_OUTPUT'] > 50:
            yield 'TCS X power is too low'
        # flow alarm
        if ezca['TCS-ITMX_CO2_INTRLK_FLOW_ALRM'] != 0:
            yield 'TCS X flow alarm'
        # IR alarm
        if ezca['TCS-ITMX_CO2_INTRLK_RTD_OR_IR_ALRM'] != 0:
            yield 'TCS X IR alarm'
            
        # FIXME: Add in TCS Y when ready
        

class OpLev(DiagTest):
    """Check OpLev sums when the suspension is aligned, and ITM(X/Y) OL is 
    functioning properly.
    
    """
    
    def test(self):
        # SUS OpLevs
        susOL = ['ITMX','ITMY','ETMX','ETMY']
        susOL2 = ['BS','PR3','SR3']
        for a in susOL:
            if ezca['GRD-SUS_'+a+'_STATE_N'] == 100:
                if ezca['SUS-'+a+'_L3_OPLEV_SUM_OUTMON'] < 500:
                    yield a+' OpLev Sum is very low'
        for b in susOL2:
            if ezca['GRD-SUS_'+b+'_STATE_N'] == 100:
                if ezca['SUS-'+b+'_M3_OPLEV_SUM_OUTMON'] < 500:
                    yield b+' OpLev Sum is very low'
        # FIXME: Uncomment when SEI OpLevs are fully operational
        # SEI OpLevs
        #seiOL = ['HAM2','HAM3','HAM4','HAM5']
        #for j in seiOL:
        #    if ezca['ISI-'+j+'_OPLEV_SUM_OUTMON'] == 0:
        #        yield j+' OpLev sum is 0' 
        
        # Make sure that ITM(X/Y) OpLev Output is ON and
        # the standard deviation of the output is not large.
        """
        for arm in ['X','Y']:
            ligo_filter = LIGOFilter('SUS-ITM{}_L2_OLDAMP_P'.format(arm),ezca)
            if not ligo_filter.is_output_on():
                yield 'ITM{} L2 OLDAMP Output is off'.format(arm)
            avgstd = avg(-30, 'SUS-ITM{}_L2_OLDAMP_P_OUTPUT'.format(arm), 1)
            if avgstd[1] > 300:
                yield 'ITM{} L2 OLDAMP output is very large'.format(arm)
        """
                
class BRS_MAIN(DiagTest):
    """Check if the BRS is Software dead or needs to be rebooted
    
    """
    inmon_nom = 0
    def test(self):
        inmon = 'ISI-GND_BRS_ETMX_RY_INMON'
        # Test for output
        if ezca['ISI-GND_BRS_ETMX_RY_OUTPUT'] == 0:
            yield "There is no BRS output"
        # Test if the drift mon is within range
        drift_mon = ezca['ISI-GND_BRS_ETMX_DRIFTMON']
        if not -10000 < drift_mon < 10000:
            yield 'BRS has drifted out of range'
        # Test if it is acting as it should
        avgstd = avg(-30,inmon,2)
        if avgstd[1] < 0.35:
            yield "BRS INMON is flatlined, possible reboot needed"
		# Test if it has rung up
        if not self.inmon_nom-100 < ezca[inmon] < self.inmon_nom+100:
            yield "The BRS has rung up"

               
BRS_DAMP_LAST_LAST = [4100]
BRS_DAMP_LAST = [4100]

class BRS_DAMPER(DiagTest):
    """Check if the BRS Damping Turntable is on and working
    
    Normal operating value is around 4100
    """
    nominal = 4100
    def test(self):
        # FIXME: "Failed to connect to NDS server" on random occasions.
        # This is pretty frequent, about 1/min.
        #gpstime = ezca['FEC-8_TIME_DIAG']
        #dat = getdata(['ISI-GND_BRS_ETMX_DAMPCTRLMON'], 30, (gpstime - 30))
        #if (max(dat[0].data) > 4220) or (min(dat[0].data) < 3980):
        #    yield "BRS Damper is ON"
            
        #if (max(dat[0].data) > 7000) or (min(dat[0].data) < 1000):
        #    yield "BRS Damper is ON"
        value = ezca['ISI-GND_BRS_ETMX_DAMPCTRLMON']
		# Checks if the damper is out of normal
        # Global assignments so the test can see the values from the last time
		# it cycled through. 
        global BRS_DAMP_LAST_LAST
        global BRS_DAMP_LAST
        if not 3000 < value < 5000:
            if not 3000 < BRS_DAMP_LAST < 5000:
                if not 3000 < BRS_DAMP_LAST_LAST < 5000:
                    yield "BRS Damper is out of range"
        # Checks if it is on
        if not self.nominal-100 < value < self.nominal+100:
            if not self.nominal-100 < BRS_DAMP_LAST < self.nominal+100:
                yield "The BRS Damper is ON"
		
        BRS_DAMP_LAST_LAST = BRS_DAMP_LAST
        BRS_DAMP_LAST = ezca['ISI-GND_BRS_ETMX_DAMPCTRLMON']
        
       
class PEM_CHANGE(DiagTest):
    """Checks if any major environment changes have occurred
    
    """
    nominal_temp = 68
    
    def test(self):
        # FIXME: These channels are still on H0
        #LVEA_TEMP = ezca['FMC-LVEA_AVTEMP_DEGF']
        #EXVEA_TEMP = ezca['FMC-EX_VEA_AVTEMP_DEGF']
        #EYVEA_TEMP = ezca['FMC-EY_VEA_AVTEMP_DEGF']
        CS_WIND = ezca['PEM-CS_WIND_ROOF_WEATHER_MPH']
        EX_WIND = ezca['PEM-EX_WIND_ROOF_WEATHER_MPH']
        EY_WIND = ezca['PEM-EY_WIND_ROOF_WEATHER_MPH']
        # VEA Temps
        #if (self.nominal_temp + 1.5 > LVEA_TEMP > self.nominal_temp - 1.5):
        #    yield "LVEA Temperature is not within range"
        #if (self.nominal_temp + 1.5 > EXVEA_TEMP > self.nominal_temp - 1.5):
        #    yield "EXVEA Temperature is not within range"
        #if (self.nominal_temp + 1.5 > EYVEA_TEMP > self.nominal_temp - 1.5):
        #    yield "EYVEA Temperature is not within range"
        
        # Wind
        if CS_WIND > 30:
            yield "High winds at the Corner Station"
        if EX_WIND > 30:
            yield "High winds at EX"
        if EY_WIND > 30:
            yield "High winds at EY"
        
        # Earthquakes
        deg_of_frees = ['X', 'Y', 'Z']
        CS_CH = 'PEM-CS_SEIS_LVEA_VERTEX_{deg_of_free}_BLRMS_30MHZ100'
        EX_CH = 'PEM-EX_SEIS_VEA_FLOOR_{deg_of_free}_BLRMS_30MHZ100'
        EY_CH = 'PEM-EY_SEIS_VEA_FLOOR_{deg_of_free}_BLRMS_30MHZ100'
        ALL_CHS = [CS_CH, EX_CH, EY_CH]
        for dof in deg_of_frees:
            for ch in ALL_CHS:
                if ezca[ch.format(deg_of_free=dof)] >= 2500:
                    yield "Earthquake! %s-Axis seismometer ringing up"%(dof)
                    break

        
class ALS(DiagTest):
    """Checks for oscillations in the ALS laser.
    This can be seen when the beatnote drops down to 0-2 MHz and
    when (in dBm) it gets above 5.
     
    Checks to see if the ALS SHG Temperature Control Servo is on
    Nominal is 'ON'
    """
    dBmBeatnotelimit = 5
    MHzBeatnotelimit = 2
    ends = ['X','Y']
    def test(self):
        # check the beatnotes to see if their signals are changed from oscillations in the laser
        for end in self.ends:
            if (ezca['ALS-%s_FIBR_A_DEMOD_RFMON'%(end)] > self.dBmBeatnotelimit) and (ezca['ALS-%s_FIBR_LOCK_BEAT_FREQUENCY'%(end)] < self.MHzBeatnotelimit):
                yield "%s Laser oscillating" % (end)
        # Make sure that the SHG servo is ON
        if ezca['ALS-C_SHG_TEC_SERVO'] == 0:
            yield "SHG Temp Control Servo 'OFF'"
        # Make sure there is no force on PLL Slow
        if ezca['ALS-C_COMM_PLLSLOW_FORCE'] != 0:
            yield 'COMM_PLLSLOW_FORCE is Forced'


class COIL_DRIVER(DiagTest):
    """Checks the coil drivers have not gone down
    
    If they stay at 0, it will bring up a notification
    """
    def test(self):
        upquad = ['LF','RT','SD','F1','F2','F3']
        upper = ['T1','T2','T3','SD','LF','RT']
        lower = ['UL','UR','LL','LR']

        quad = {'M0':upquad,'R0':upquad,'L1':lower,'L2':lower}
        bs = {'M1':upquad,'M2':lower}
        triple = {'M1':upper,'M2':lower,'M3':lower}
        single = {'M1':upper}

        suspensions = {'BS':bs,'ITMX':quad,'ITMY':quad,'ETMX':quad,'ETMY':quad,
        'PRM':triple,'PR2':triple,'PR3':triple,'MC1':triple,'MC2':triple,'MC3':triple,
        'SRM':triple,'SR2':triple,'SR3':triple,'OMC':single,'TMSX':{'M1':upquad},'TMSY':{'M1':upquad}}
        
        for sus,level in suspensions.items():
            for lev,osem in level.items():
                for ose in osem:
                    if ezca['SUS-%s_%s_NOISEMON_%s_MON'%(sus,lev,ose)] == 0:   
                        avgstd = avg(-2,'SUS-%s_%s_NOISEMON_%s_MON'%(sus,lev,ose))
                        if avgstd == 0:
                            yield '%s_%s_%s noisemon is dead' % (sus,lev,ose)



class SW_WD(DiagTest):
    """If a SoftWare WatchDog is tripped
    
    We will know
    """
    def test(self):
        hams = ['HAM2','HAM3','HAM4','HAM5','HAM6']
        chambers = ['BS','ITMX','ITMY','ETMX','ETMY']
        extra = ['TMSY','TMSX','OMC','MC1','MC2','MC3','SRM','SR2','SR3','PRM','PR2','PR3']
        seis = hams + chambers
        sus = chambers + extra
        total = {'SEI':seis, 'SUS':sus}
        for sub, value in total.iteritems():
            for j in value:
                if not ezca['IOP-'+sub+'_'+j+'_DACKILL_STATE'] == 1:
                    yield " %s %s Tripped" % (sub,j)


class SUS_PUM_WD(DiagTest):
    """Checks the RMS WatchDogs
    
    """
    def test(self):
        quads = ['ETMX','ETMY','ITMX','ITMY']
        for j in quads:
            if int(ezca['SUS-'+j+'_BIO_L2_MON']) & 2 != 2:
                yield "{} RMS UL WD has tripped".format(j)
            if int(ezca['SUS-'+j+'_BIO_L2_MON']) & 32 != 32:
                yield "{} RMS LL WD has tripped".format(j)
            if int(ezca['SUS-'+j+'_BIO_L2_MON']) & 512 != 512:
                yield "{} RMS UR WD has tripped".format(j)
            if int(ezca['SUS-'+j+'_BIO_L2_MON']) & 8192 != 8192:
                yield "{} RMS LR WD has tripped".format(j)


class SUS_WD(DiagTest):
    """Checks SUS WatchDogs and osem input filter inmon on the quads"""
    def test(self):
        suspensions = {
            'BS':['M1','M2'],
            'ITMX':['M0','R0','L1','L2'],
            'ITMY':['M0','R0','L1','L2'],
            'ETMX':['M0','R0','L1','L2'],
            'ETMY':['M0','R0','L1','L2'],
            'PRM':['M1','M2','M3'],
            'PR2':['M1','M2','M3'],
            'PR3':['M1','M2','M3'],
            'MC1':['M1','M2','M3'],
            'MC2':['M1','M2','M3'],
            'MC3':['M1','M2','M3'],
            'SRM':['M1','M2','M3'],
            'SR2':['M1','M2','M3'],
            'SR3':['M1','M2','M3'],
            'OMC':['M1'],
            'TMSX':['M1'],
            'TMSY':['M1'],
            'RM1':['M1'],
            'RM2':['M1'],
            'IM1':['M1'],
            'IM2':['M1'],
            'IM3':['M1'],
            'IM4':['M1'],
            'OM1':['M1'],
            'OM2':['M1'],
            'OM3':['M1']
            }
        gen_osems = ['UL','LL','UR','LR']
        top_stage = ['F1','F2','F3','LF','RT','SD']
        quads = {
            'ITMX':{'M0':top_stage,'L1':gen_osems,'L2':gen_osems},
            'ITMY':{'M0':top_stage,'L1':gen_osems,'L2':gen_osems},
            'ETMX':{'M0':top_stage,'L1':gen_osems,'L2':gen_osems},
            'ETMY':{'M0':top_stage,'L1':gen_osems,'L2':gen_osems},
            }
        for sus, level in suspensions.iteritems():
            for lev in level:
                if not ezca['SUS-{}_{}_WDMON_STATE'.format(sus,lev)] == 1:
                    yield '{} {} WD Tripped'.format(sus,lev)
        # Osems input filter inmon
        for sus, level in quads.iteritems():
            for lev, osems in level.iteritems():
                for osem in osems:  
                    if ezca['SUS-{}_{}_OSEMINF_{}_INMON'.format(sus, lev, osem)] < 5:
                        yield 'SUS {} {} osem filter input inmon is dead'.format(sus,lev)
                        break

class SEI_STATE(DiagTest):
    """
    Checks if the SEI systems are in their nominal configuration
    
    Also checks if the WD saturation counts are are greater than some percent of the limit.

    """
    def test(self):
        hams = ['HAM2', 'HAM3', 'HAM4', 'HAM5', 'HAM6']
        quads = ['ITMX', 'ITMY', 'ETMX', 'ETMY']
        sei = {
            'ham': ['HAM2', 'HAM3', 'HAM4', 'HAM5', 'HAM6'],
            'bsc_st1': ['ITMX', 'ITMY', 'ETMX', 'ETMY'],
            'bsc_st2': ['ITMX', 'ITMY', 'ETMX', 'ETMY'],
            }
        acts = {
            'ham': ['CPS', 'GS13', 'L4C', 'ACT'],
            'bsc_st1': ['CPS', 'T240', 'L4C', 'ACT'],
            'bsc_st2': ['CPS', 'GS13', 'ACT'],
            }
        count_percent = 0.75
        #####
        # Nominal state
        for i in hams:
            if not ezca['GRD-SEI_{}_STATE'.format(i)] == 'ISOLATED':
                yield '{} is not in its nominal state'.format(i)
        for x in quads:
            if not ezca['GRD-SEI_{}_STATE'.format(x)] == 'FULLY_ISOLATED':
                yield '{} is not in its nominal state'.format(x)
        # FIXME: BS isnt using st2 iso yet so its nominal is at 100
        if not ezca['GRD-SEI_BS_STATE'] == 'ISOLATED_DAMPED':
            yield "BS is not in its nominal state"
        # HAM1
        if not ezca['GRD-HPI_HAM1_STATE'] == 'ROBUST_ISOLATED':
            yield "HAM1 is not in its nominal state"

        #####
        # Saturation counts
        for chamber_type, chambers in sei.iteritems():
            if chamber_type == 'ham':
                for chamber in chambers:
                    for act in acts[chamber_type]:
                        if (ezca['ISI-{}_WD_{}_SAT_COUNT'.format(chamber,act)]
                            > count_percent * ezca['ISI-{}_WD_{}_SAFECOUNT'.format(chamber,act)]):
                            yield 'ISI {} WD saturation count is greater than 75% of max'.format(chamber)
            elif chamber_type == 'bsc_st1':
                for chamber in chambers:
                    for act in acts[chamber_type]:
                        if (ezca['ISI-{}_ST1_WD_{}_SAT_COUNT'.format(chamber,act)]
                            > count_percent * ezca['ISI-{}_ST1_WD_{}_SAFECOUNT'.format(chamber,act)]):
                            yield 'ISI {} ST1 WD saturation count is greater than 75% of max'.format(chamber)
            elif chamber_type == 'bsc_st2':
                for chamber in chambers:
                    for act in acts[chamber_type]:
                        if (ezca['ISI-{}_ST2_WD_{}_SAT_COUNT'.format(chamber,act)]
                            > count_percent * ezca['ISI-{}_ST2_WD_{}_SAFECOUNT'.format(chamber,act)]):
                            yield 'ISI {} ST2 WD saturation count is greater than 75% of max'.format(chamber)
        
        
        

class ESD_DRIVER(DiagTest):
    """Checks if the ESD Drivers have turned off
    
    """
    threshold = 70
    railed = 16200
    def test(self):
        arms = ['X','Y']
        for arm in arms:
            if -self.threshold < ezca['IOP-SUSAUX_E%s_MADC4_EPICS_CH0'%arm] < self.threshold:
                # ESD X is turned off in the state LOWNOISE_ESD_ETMY(515)
                if arm == 'X':
                    if not ezca['GRD-ISC_LOCK_STATE_N'] >= 515:
                        yield 'ESD {} driver is OFF'.format(arm)
                else:
                    yield 'ESD {} driver is OFF'.format(arm)
            avg_std = avg(-5, 'IOP-SUSAUX_E{}_MADC4_EPICS_CH1'.format(arm), 2)
            if (self.railed+600 > abs(avg_std[0]) > self.railed-600) and avg_std[1] < 1000:
                yield 'ESD {} is railed'.format(arm)
            

class TIDAL_LIMITS(DiagTest):
    """Checks if the tidal limits have been reached

    If the specified channel reaches to within 10% of
    the limit value, it will notfiy
    """
    def test(self):
        arms = ['X','Y']
        place = ['ARM','TIDAL','COMM']
        for axis in arms:
            for j in place:
                if percent_diff(ezca['LSC-%s_%s_CTRL_LIMIT'%(axis,j)], ezca['LSC-%s_%s_CTRL_OUT16'%(axis,j)]) <= 10:
                #if (ezca['LSC-%s_%s_CTRL_LIMIT'%(axis,j)] - abs(ezca['LSC-%s_%s_CTRL_OUT16'%(axis,j)]))/ezca['LSC-%s_%s_CTRL_LIMIT'%(axis,j)] <= .10:
                    yield '%s_%s_CTRL ouput is within 10 percent of the limit' %(axis,j)


class RING_HEATERS(DiagTest):
    """Checks if the ring heaters are on in the end stations"""
    def test(self):
        for dof in ['X','Y']:
            upper = ezca['TCS-ETM{}_RH_SETUPPERPOWER'.format(dof)]
            lower = ezca['TCS-ETM{}_RH_SETLOWERPOWER'.format(dof)]
            # Check its on
            if lower == 0 and upper == 0:
                yield 'ETM{} Ring heater is OFF'.format(dof)
            # Check that not only one half is on
            elif (upper == 0 and lower != 0) or (upper != 0 and lower == 0):
                if upper == 0:
                    yield 'ETM{} top is OFF, bottom ON'.format(dof)
                if lower == 0:
                    yield 'ETM{} bottom is OFF, top is ON'.format(dof)
            # Percent difference of the top half and the bottom half power
            elif percent_diff(upper, lower) >= 30:
                yield 'ETM{} top and bottom powers are not similar'.format(dof)
            

beck_dict = {
        'C1_PLC1' : 0,
        'X1_PLC1' : 0,
        'Y1_PLC1' : 0}
class BECKHOFF(DiagTest):
    """Checks a few random Beckhoff channels to see if it has crashed"""
    def test(self):
        for loc in ['C','X','Y']:
            if not ezca['SYS-ETHERCAT_{}1PLC1_ELAPSEDTIME'.format(loc)] > beck_dict['{}1_PLC1'.format(loc)]:
                yield 'Beckhoff crashed'
            
            beck_dict['{}1_PLC1'.format(loc)] = ezca['SYS-ETHERCAT_{}1PLC1_ELAPSEDTIME'.format(loc)]
        

class HWS(DiagTest):
    """Check if SLEDS are on during data taking"""
    def test(self):
        # HWSIX_SLED
        SLED_switch_IX = ezca['TCS-ITMX_HWS_SLEDSHUTDOWN']
        if not SLED_switch_IX == 1:
            yield "HWSIX SLED is ON" 
#        # HWSIX_CAMERA
#        CAMERA_switch_IX = ezca['TCS-ITMX_HWS_DALSACAMERASWITCH']
#        if CAMERA_switch_IX == 1:
#            yield "HWSIX CAMERA is ON" 
#        # HWSIX_FRAMEGRABBER
#        FRAME_switch_IX = ezca['TCS-ITMX_HWS_RCXCLINKSWITCH']
#        if FRAME_switch_IX == 1:
#            yield "HWSIX FRAME GRABBER is ON" 

        # HWSIY_SLED
        SLED_switch_IY = ezca['TCS-ITMY_HWS_SLEDSHUTDOWN']
        if not SLED_switch_IY == 1:
            yield "HWSIY SLED is ON"
#        # HWSIY_CAMERA
#        CAMERA_switch_IY = ezca['TCS-ITMY_HWS_DALSACAMERASWITCH']
#        if CAMERA_switch_IY == 1:
#            yield "HWSIY CAMERA is ON" 
#        # HWSIY_FRAMEGRABBER
#        FRAME_switch_IY = ezca['TCS-ITMY_HWS_RCXCLINKSWITCH']
#        if FRAME_switch_IY == 1:
#            yield "HWSIY FRAME GRABBER is ON" 

#        # HWSEX_CAMERA
#        CAMERA_switch_EX = ezca['TCS-ETMX_HWS_DALSACAMERASWITCH']
#        if CAMERA_switch_EX == 1:
#            yield "HWSEX CAMERA is ON" 
#        # HWSEX_FRAMEGRABBER
#        FRAME_switch_EX = ezca['TCS-ETMX_HWS_RCXCLINKSWITCH']
#        if FRAME_switch_EX == 1:
#            yield "HWSEX FRAME GRABBER is ON" 

#        # HWSEY_CAMERA
#        CAMERA_switch_EY = ezca['TCS-ETMY_HWS_DALSACAMERASWITCH']
#        if CAMERA_switch_EY == 1:
#            yield "HWSEY CAMERA is ON" 
#        # HWSEY_FRAMEGRABBER
#        FRAME_switch_EY = ezca['TCS-ETMY_HWS_RCXCLINKSWITCH']
#        if FRAME_switch_EY == 1:
#            yield "HWSEY FRAME GRABBER is ON" 


##################################################
# Useful functions

def RMS(current_channel):
    """Takes the RMS by taking the square root of the average^2 + stdev^2"""
    avg_stddev = avg(-5,current_channel,2)
    rms = np.sqrt(np.square(avg_stddev[0]) + np.square(avg_stddev[1]))
    return rms

def percent_diff(first,second):
    """Finds and returns the percent difference betweent the two values given"""
    local_avg = float(first + second)/2
    diff = abs(first - second)/local_avg
    percent = diff * 100
    return percent


##################################################
# Garage


# This test is just creating alarm noise,
# a further investigation with the SUS team is needed

#class SUS_OUT_OF_RANGE(DiagTest):
#    """Checks if a SUS is out of range
#    
#    This reads off of the .HIHI and .LOLO values.
#    Should yield a notification if a SUS is red on the SUS_DRIFT_MONITOR.
#    """
#    def test(self):
#        suspensions = {'BS':['M1'],'ITMX':['M0'],'ITMY':['M0'],'ETMX':['M0'],'ETMY':['M0'],
#        'PRM':['M1'],'PR2':['M1'],'PR3':['M1'],'MC1':['M1'],'MC2':['M1'],'MC3':['M1'],
#        'SRM':['M1'],'SR2':['M1'],'SR3':['M1'],'OMC':['M1'],'TMSX':['M1'],'TMSY':['M1']
#        }
#        freedoms = ['P','V','Y']
#        
#        suspensions_alt = {'RM1':['M1'],'RM2':['M1'],'IM1':['M1'],'IM2':['M1'],'IM3':['M1'],'IM4':['M1'],'OM1':['M1'],'OM2':['M1'],'OM3':['M1']}
        
#        freedoms_alt = ['L','P','Y']

#        for i in suspensions:
#            for j in suspensions[i]:
#                for k in freedoms:
#                    if ezca['SUS-%s_%s_DAMP_%s_INMON'%(i,j,k)] > ezca['SUS-%s_%s_DAMP_%s_INMON.HIHI'%(i,j,k)]:
#                        yield '%s %s Greater than range value' % (i,k)
#                    if ezca['SUS-%s_%s_DAMP_%s_INMON'%(i,j,k)] < ezca['SUS-%s_%s_DAMP_%s_INMON.LOLO'%(i,j,k)]:
#                        yield '%s %s Less than range value' % (i,k)

#        for i in suspensions_alt:
#            for j in suspensions_alt[i]:
#                for k in freedoms_alt:
#                    if ezca['SUS-%s_%s_DAMP_%s_INMON'%(i,j,k)] > ezca['SUS-%s_%s_DAMP_%s_INMON.HIHI'%(i,j,k)]:
#                        yield '%s %s Greater than range value' % (i,k)
#                    if ezca['SUS-%s_%s_DAMP_%s_INMON'%(i,j,k)] < ezca['SUS-%s_%s_DAMP_%s_INMON.LOLO'%(i,j,k)]:
#                        yield '%s %s Less than range value' % (i,k)                      




#######
# This test is temporarily unavailable due to extremely long exec time.
"""
class BLRMS_MATRIX(DiagTest):
    Checks if the BLRMS matrices are mostly green
    
    This is done by taking the average of the numerical value of all 36 channels in each matrix
    If the current average is above 0.3 as well as from the previous cycle, then a notification appears
      
    def test(self):
        freq = ['30M_100M','100M_300M','300M_1','1_3','3_10','10_30']
        dof = ['X','Y','Z','RX','RY','RZ']
        dofzip = {'X':freq,'Y':freq,'Z':freq,'RX':freq,'RY':freq,'RZ':freq}
        hams = ['HAM2','HAM3','HAM4','HAM5','HAM6']
        bscs = ['ITMY','BS','ITMX','ETMX','ETMY']
        if 1 < 0:
            yield "You broke math!"
        
        for ham in hams:
            channels = []
            for d,f in dofzip.iteritems():
                for fre in freq:
                    channels.append('ISI-'+ham+'_BLRMS_LOG_'+d+'_'+fre)

            avg_avg = (sum(avg(-5,channels))/len(channels))
            if avg_avg > 0.3:
                yield ham+' is not in good shape'
        
        for bsc in bscs:
            #Stage 1
            channels_st1 = []
            for d,f in dofzip.iteritems():
                for fre in freq:
                    channels_st1.append('ISI-'+bsc+'_ST1_FFB_LOG_'+d+'_'+fre)
            avg_avg_st1 = (sum(avg(-5,channels_st1))/len(channels_st1))
            if avg_avg_st1 > 0.3:
                yield bsc+' Stage 1 is not in good shape'
            #Stage 2
            channels_st2 = []
            for d,f in dofzip.iteritems():
                for fre in freq:
                    channels_st2.append('ISI-'+bsc+'_ST2_BLND_LOG_'+d+'_'+fre)
            avg_avg_st2 = (sum(avg(-5,channels_st2))/len(channels_st2))
            if avg_avg_st2 > 0.3:
                yield bsc+' Stage 2 is not in good shape'
        
"""

"""
class ISS_SATURATION(DiagTest):
    Checks if the ISS has been satruated recently
    
    If the ISS has saturated within 20min, it will notify
    
    def test(self):
        sat_min = ezca['PSL-ISS_SAT_MIN']
        sat_hour = ezca['PSL-ISS_SAT_HOUR']
        sat_day = ezca['PSL-ISS_SAT_DAY']
        if sat_day == 0 and sat_hour == 0 and sat_min <= 20:
            yield 'The ISS saturated %s min ago' % (sat_min)
"""

